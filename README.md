# **ABOUT**

This repository contains the code for the R Shiny companion application of my Medium post: R Shiny authentication (incl. demo app).

Please comment on the post in case of any feedback/ question.

Many thanks!

Thomas
